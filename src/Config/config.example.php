<?php

return [
    /*
    * Discord Bot Settings
    */
    
    'discord' =>
    [
        # Bot API key
        'bot_token' => 'YOUR KEY',

        # Username for bot
        'bot_username' => 'SomeBot',

        # URL to bot avatar
        'bot_avatar' => 'SomeBot',

        # Prefix for the bot to react to
        'bot_prefix' => '!',
    ],
];
