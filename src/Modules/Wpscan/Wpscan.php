<?php

namespace Pillus\Secbot\Modules\Wpscan;

use Pillus\Secbot\Helpers\Apirequest;

class Wpscan
{
    /**
     * Set the required parameters when initalized
     */
  
    public function __construct()
    {
        $this->apirequest = new Apirequest;
        $this->baseurl = 'https://wpvulndb.com/api/v2/';
    }
    
    /**
    * Check a Wordpress version for information on wpvulndb.com
    */

    public function versionSearch($version)
    {
        $data = [
            'base_uri' => sprintf($this->baseurl . 'wordpresses/%s', $version),
        ];

        return $this->apirequest->grab($data, 'GET');
    }

    /**
    * Check a Wordpress plugin for information on wpvulndb.com
    */

    public function pluginSearch($plugin)
    {
        $data = [
            'base_uri' => sprintf($this->baseurl . 'plugins/%s', $plugin),
        ];

        return $this->apirequest->grab($data, 'GET');
    }
};
