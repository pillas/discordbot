<?php

namespace Pillus\Secbot\Modules\Haveibeenpwned;

use Pillus\Secbot\Helpers\Apirequest;

class Haveibeenpwned
{
    protected $config;

    /**
    * Set some parameters when class is initialized
    */

    public function __construct()
    {
        $this->baseurl = 'https://haveibeenpwned.com/api/v2/';
        $this->apirequest = new Apirequest;
    }
    
    /**
    * Check an account for possible breaches
    */

    public function accountSearch($account)
    {
        $data = [
            'base_uri' => sprintf($this->baseurl . '/breachedaccount/%s', urlencode($account)),
        ];

        return $this->apirequest->grab($data, 'GET');
    }

    /**
    * List all public credentials listed for an email
    */

    public function emailPasteSearch($email)
    {
        $data = [
            'base_uri' => sprintf($this->baseurl . 'pasteaccount/%s', urlencode($email)),
        ];

        return $this->apirequest->grab($data, 'GET');
    }
};
