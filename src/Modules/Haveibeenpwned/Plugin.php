<?php

namespace Pillus\Secbot\Modules\Haveibeenpwned;

use Pillus\Secbot\Modules\Haveibeenpwned\Haveibeenpwned;

/**
* This module checks haveibeenpwned.com for leaks related to the choosen account
* or email address, and returns with the leaked information
**/

class Plugin
{

    /**
     * @var discord
     */
    protected $discord;

    /**
     * @var Haveibeenpwned
     */
    protected $service;

    public function __construct()
    {
        $this->service = new Haveibeenpwned;
    }

    public function setup($discord)
    {
        $this->discord = $discord;

        return $this;
    }

    /**
    * Initiates all available commands used by the Haveibeenpwned module.
    **/

    public function init()
    {
        $command = $this->discord->registerCommand('haveibeenpwned', function () {
        });
        
        $command->registerSubCommand('account', function ($message, $params) {
            $reply = self::handleAccountSearch($message->content);
            return $reply;
        });
        
        $command->registerSubCommand('email', function ($message, $params) {
            $reply = self::handleEmailPasteSearch($message->content);
            return $reply;
        });
    }

    /**
    * Searches a specific username or account name for leaked information
    **/

    public function handleAccountSearch($message)
    {
        $message = explode(' ', trim($message));
        $account = $message[2];
        $results = $this->service->AccountSearch($account);
        
        # Sanity check for the returning response from the API.
        if (is_bool($results) || count($results) === 0) {
            $reply[] = sprintf('*No Leaks found for this account*');
            $message = implode(PHP_EOL, $reply);
            return $message;
        }

        # Get the title and date of the leak, and confirms if it is verified
        else {
            foreach ($results as $result) {
                $title = array_get($result, 'Title');
                $date = array_get($result, 'BreachDate');
                $verified = array_get($result, 'IsVerified');

                $reply[] = sprintf('Title: %s', $title);
                $reply[] = sprintf('Breach Date: %s', $date);
                $reply[] = sprintf('Verified leak?: %s', $verified ? 'yes':'no') . PHP_EOL;
            }

            $message = implode(PHP_EOL, $reply);
            return $message;
        }
    }

    /**
    * Lists all sources a email address is found in different database breaches
    **/

    public function handleEmailPasteSearch($message)
    {
        $message = explode(' ', trim($message));
        $email = $message[2];
        $results = $this->service->emailPasteSearch($email);

        # Sanity check for the returning response from the API.
        if (is_bool($results) || count($results) === 0) {
            $reply[] = sprintf('No Leaks found for this email account');
            $message = implode(PHP_EOL, $reply);
            return $message;
        } else {
            $reply[] = sprintf('Your email has been leaked %s Times', count($results));

            foreach ($results as $result) {
                $title = array_get($result, 'Title');
                $source = array_get($result, 'Source');
                $resultId = array_get($result, 'Id');
                $date = date('d/m/Y', strtotime(array_get($result, 'Date')));

                if (count(array_get($result, 'Title')) > 0) {
                    $reply[] = sprintf('*Title:* %s', $title);
                } else {
                    $reply[] = sprintf('*Title:* No title found');
                }
                    
                $reply[] = sprintf('*Source:* %s', $source);

                # If it is a pastebin link, we will have to append the url manually
                if ($source == 'Pastebin') {
                    $reply[] = sprintf('*URL:* http://www.pastebin.com/%s', $resultId) . PHP_EOL;
                } else {
                    $reply[] = sprintf('*URL:* %s', $resultId);
                }

                $reply[] = sprintf('*Published:* %s', $date) . PHP_EOL;
            }
            
            $message = implode(PHP_EOL, $reply);
            return $message;
        }
    }
}
