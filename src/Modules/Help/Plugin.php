<?php

namespace Pillus\Secbot\Modules\Help;

use Pillus\Secbot\Modules\Help\Help;

/**
* This modules handles all help commands used by the bot
**/

class Plugin
{

    /**
     * @var Botman
     */
    protected $discord;

    /**
     * @var Help
     */
    protected $service;

    public function __construct()
    {
        $this->service = new Help;
    }

    public function setup($discord)
    {
        $this->discord = $discord;

        return $this;
    }

    public function init()
    {
        $this->discord->registerCommand('listcommands', function ($message, $params) {
            $reply = self::handleHelp($message->content);
            return $reply;
        });
    }

    /*
     * List all commands available
     */

    public function handleHelp($discord)
    {
        $results = $this->service->listCommands();
        $reply = [
            'The current supported commands are:',
        ];

        foreach ($results as $result) {
            $reply[] = $result . PHP_EOL;
        }

        $message = implode(PHP_EOL, $reply);
        return $message;
    }
}
