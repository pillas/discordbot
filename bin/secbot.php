<?php

/**
* Grab the composer autoloader
*/

include __DIR__.'/../vendor/autoload.php';

use Discord\DiscordCommandClient;

/**
* Import the custom plugins
*/

use Pillus\Secbot\Modules\Wpscan\Plugin as WpscanPlugin;
use Pillus\Secbot\Modules\Help\Plugin as HelpPlugin;
use Pillus\Secbot\Modules\Shodan\Plugin as ShodanPlugin;
use Pillus\Secbot\Modules\Virustotal\Plugin as VirustotalPlugin;
use Pillus\Secbot\Modules\Haveibeenpwned\Plugin as HaveibeenpwnedPlugin;

/**
* Load config file for the bot itself
*/

$config = require __DIR__.'/../src/Config/config.php';


/**
* Instantiate Discord class
*/

$discord = new DiscordCommandClient([
    'token' => array_get($config, 'discord.bot_token'),
    'prefix' => array_get($config, 'discord.bot_prefix'),
    #'name' => array_get($config, 'discord.bot_username'),
    #'avatar' => array_get($config, 'discord.bot_avatar'),
    #'verified' => true,
]);



$plugins = [
    (new HelpPlugin)->setup($discord),
    (new ShodanPlugin)->setup($discord),
    (new WpscanPlugin)->setup($discord),
    (new VirusTotalPlugin)->setup($discord),
    (new HaveibeenpwnedPlugin)->setup($discord),
];

/**
* Initiate all plugins
*/

foreach ($plugins as $plugin) {
    $plugin->init();
}

$discord->run();
