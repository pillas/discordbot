<?php

namespace Pillus\Secbot\Modules\Shodan;

use Pillus\Secbot\Modules\Shodan\Shodan;

/**
* This module intergrates with the Shodan.io API to allow us to query it's database
**/

class Plugin
{

    /**
     * @var discord
     **/
    protected $discord;

    /**
     * @var Shodan
     **/
    protected $service;

    public function __construct()
    {
        $this->service = new Shodan;
    }

    public function setup($discord)
    {
        $this->discord = $discord;

        return $this;
    }

    /**
    * Initiates all available commands used by the Shodan module.
    **/

    public function init()
    {
        $command = $this->discord->registerCommand('shodan', function () {
        });
        
        $command->registerSubCommand('ip', function ($message, $params) {
            $reply = self::handleShIpSearch($message->content);
            return $reply;
        });
        
        $command->registerSubCommand('query', function ($message, $params) {
            $reply = self::handleShQuerySearch($message->content);
            return $reply;
        });

        $command->registerSubCommand('heartbleed', function ($message, $params) {
            $reply = self::handleShHbSearch($message->content);
            return $reply;
        });

        $command->registerSubCommand('vuln', function ($message, $params) {
            $reply = self::handleShVulnSearch($message->content);
            return $reply;
        });

        $command->registerSubCommand('listqueries', function ($message, $params) {
            $reply = self::handleShListQuerySearch($message->content);
            return $reply;
        });
    }

    /**
    * Handles searching for IP Addresses, then formats the output before sending it to
    * the destination.
    **/

    public function handleShIpSearch($message)
    {
        $message = explode(' ', trim($message));
        $ip = $message[2];
        $results = $this->service->ipSearch($ip);

        # Sanity check for the returning response from the API.
        if (is_bool($results) || count(array_get($results, 'data')) === 0) {
            $reply[] = sprintf('*No results found for:* %s', $ip);
            $message = implode(PHP_EOL, $reply);
            return $message;
        }

        # Get all the returning information from the IP search and builds a reply.
        else {
            $target = array_get($results, 'data.0.os');
            $company = array_get($results, 'data.0.asn');
            $isp = array_get($results, 'data.0.isp');
            $country = array_get($results, 'country_name');
            $results = array_get($results, 'data', []);

            if (count($target) === 0) {
                $target = 'Unknown';
            }
                    
            $reply = [
                sprintf('*Your Results for*: %s', $ip),
                sprintf('*Target is running on*: %s', $target),
                sprintf('*Company*: %s', $company),
                sprintf('*ISP*: %s', $isp),
                sprintf('*Originates from*: %s', $country),
                sprintf('*Ports open are*:'),
            ];
            
            # Gets all Ports and their products.
            foreach ($results as $result) {
                $transport = strtoupper(array_get($result, 'transport'));
                $port = array_get($result, 'port');
                $product = array_get($result, 'product');

                if (count($product) === 0) {
                    $product = 'Not known';
                }

                $reply[] = sprintf('*%s*: %s - %s', $transport, $port, $product);
            }

            $reply[] = sprintf('*More results can be found on*: https://www.shodan.io/host/%s', $ip);
        }

        $message = implode(PHP_EOL, $reply);
        return $message;
    }

    /**
    * Handles running queries, then formats the output before sending it to the
    * destination.
    **/

    public function handleShQuerySearch($message, $facets = null)
    {
        $message = explode(' ', trim($message));
        $query = $message[2];
        $results = $this->service->querySearch($query);

        # Sanity check for the returning response from the API.
        if (is_bool($results) || count(array_get($results, 'matches')) === 0) {
            $reply [] = sprintf('*No results found for:* %s', $query);
            $message = implode(PHP_EOL, $reply);
            return $message;
        }
        
        # Gets the returning IP addresses, ports and organization names.
        else {
            $reply = [
                sprintf('*30 newest results for:* %s', $query) . PHP_EOL,
            ];

            foreach (array_get($results, 'matches', []) as $match) {
                $reply[] = sprintf('*IP* %s - *Port:* %s - *Org:* %s',
                    array_get($match, 'ip_str'),
                    array_get($match, 'port'),
                    array_get($match, 'org'));
            }
        }

        $message = implode(PHP_EOL, array_slice($reply, 0, 31));
        return $message;
    }

    /**
    * Handles the Heartbleed searches, then formats the output before sending it to the
    * destination.
    **/

    public function handleShHbSearch($message)
    {
        $message = explode(' ', trim($message));
        $target = $message[2];
        $results = $this->service->hbSearch($target);

        # Sanity check for the returning response from the API.
        if (is_bool($results) || count(array_get($results, 'data')) === 0) {
            $reply [] = sprintf('*No results found for:* %s', $target);
            $message = implode(PHP_EOL, $reply);
            return $message;
        }

        # Checks if the returning results has any information, if it does, it is vulnerable.
        elseif (array_get($results, 'data.0.opts.heartbleed') > 0) {
            $vuln = 'Vulnerable';
            $reply[] = sprintf('*Target:* %s is *%s*', $target, $vuln);
            $message = implode(PHP_EOL, $reply);
            return $message;
        }

        # If it has data, but nothing related to heartbleed.
        else {
            $vuln = 'Not Vulnerable';
            $reply[] = sprintf('*Target:* %s is *%s*', $target, $vuln);
            $message = implode(PHP_EOL, $reply);
            return $message;
        }
    }

    /**
    * Handles the vulnerability searches, then formats the output before sending it to
    * the destination.
    **/

    public function handleShVulnSearch($message)
    {
        $message = explode(' ', trim($message));
        $target = $message[2];
        $results = $this->service->vulnSearch($target);

        # Sanity check for the returning response from the API.
        if (is_bool($results) || count(array_get($results, 'vulns')) === 0) {
            $reply[] = sprintf('*No results found for:* %s', $target);
            $message = implode(PHP_EOL, $reply);
            return $message;
        }
        
        # Get's the returning vulnerabilities for a specific IP Address.
        else {
            $vulns = array_get($results, 'vulns');
            $reply = [
                sprintf('*Results for* %s:', $target),
                sprintf('*%s Vulnerabilitie(s) was found*', count($vulns)),
            ];
            
            foreach ($vulns as $vuln) {
                $reply[] = sprintf('*CVE:* %s', $vuln);
            }

            $message = implode(PHP_EOL, $reply);
            return $message;
        }
    }

    /**
    * Handles the listing of commonly used queries, then formats the output before sending it to the
    * destination.
    **/

    public function handleShListQuerySearch($message)
    {
        $results = $this->service->listQuerySearch();
        
        # Sanity check for the returning response from the API.
        if (is_bool($results) || count(array_get($results, 'matches')) === 0) {
            $reply [] = sprintf('*A problem occured*');
            $message = implode(PHP_EOL, $reply);
            return $message;
        }

        # Returns commonly used queries.
        else {
            $reply = [
                sprintf('*Popular queries are:* ')
            ];
            
            foreach (array_get($results, 'matches', []) as $query) {
                $reply[] = sprintf('*Query:* %s', array_get($query, 'query'));
            }

            $message = implode(PHP_EOL, $reply);
            return $message;
        }
    }
}
