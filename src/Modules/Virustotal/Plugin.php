<?php

namespace Pillus\Secbot\Modules\Virustotal;

use Pillus\Secbot\Modules\Virustotal\Virustotal;

/**
* This module handles all communication with Virustotal, including URL and IP searches,
* and gathering reports from finalized scans.
**/

class Plugin
{

    /**
     * @var Botman
     */
    protected $discord;

    /**
     * @var Virustotal
     */
    protected $service;

    public function __construct()
    {
        $this->service = new Virustotal;
    }

    public function setup($discord)
    {
        $this->discord = $discord;

        return $this;
    }

    /**
    * Initiates all available commands used by the Shodan module.
    **/

    public function init()
    {
        $command = $this->discord->registerCommand('vt', function () {
        });
        
        $command->registerSubCommand('ip', function ($message, $params) {
            $reply = self::handleIpSearch($message->content);
            return $reply;
        });

        $command->registerSubCommand('hash', function ($message, $params) {
            $reply = self::handleHashSearch($message->content);
            return $reply;
        });
        
        $command->registerSubCommand('url', function ($message, $params) {
            $reply = self::handleUrlSearch($message->content);
            return $reply;
        });
    }

    /**
    * Checks Virustotal if the IP address is malicious.
    **/

    public function handleIpSearch($message)
    {
        $message = explode(' ', trim($message));
        $ip = $message[2];
        $results = $this->service->ipSearch($ip);
        
        # Sanity check for the returning response from the API.
        if (is_bool($results) || count(array_get($results, 'detected_urls')) === 0) {
            $reply[] = sprintf('*No malicious domains was found on %s*', $ip);
            $message = implode(PHP_EOL, $reply);
            return $message;
        }

        # Retrieves the results from the IP address
        else {
            $detected = array_get($results, 'detected_urls');

            $reply[] = sprintf('*Your Results for:* %s', $ip);
            $reply[] = sprintf('*Malicious domains found:* %s', count($detected));
            
            foreach ($detected as $url) {
                $url = array_get($url, 'url');
                $reply[] = sprintf('*URL:* %s', $url);
            }
                
            $reply[] = sprintf('*For more information, go to:* https://www.virustotal.com/en/ip-address/%s/information', $ip);

            $message = implode(PHP_EOL, array_slice($reply, 0, 31));
            return $message;
        }
    }

    /**
    * Checks Virustotal if the file hash has been scanned before
    **/

    public function handleHashSearch($message)
    {
        $message = explode(' ', trim($message));
        $hash = $message[2];
        $results = $this->service->hashSearch($hash);
        
        # Sanity check for the returning response from the API.
        if (is_bool($results) || count(array_get($results, 'positives')) === 0) {
            $reply[] = sprintf('*No report has been found on file with hash: %s*', $hash);
            $message = implode(PHP_EOL, $reply);
            return $message;
        }

        # Retrieves the results from the hash
        else {
            $detected = array_get($results, 'positives');
            $total = array_get($results, 'total');
            $scan_id = array_get($results, 'scan_id');
            $scans = array_get($results, 'scans');

            $reply[] = sprintf('Your Results for hash: %s', $hash);
            $reply[] = sprintf('*%s of %s found this hash to be malicious.* ', $detected, $total);
            $reply[] = sprintf('*Only detected will be listed here due to discord limit, please visit link for full version*' . PHP_EOL);
            
            foreach ($scans as $scan => $value) {
                if (array_get($value, 'detected') === true) {
                    $reply[] = sprintf('%s | Malware Name: %s', $scan, array_get($value, 'result'));
                }
            }
                
            $reply[] = sprintf(PHP_EOL . '*For more information, go to:* %s', array_get($results, 'permalink'));
            $message = implode(PHP_EOL, $reply);
            return $message;
        }
    }

    /**
    * Checks Virustotal if the URL address is malicious.
    **/

    public function handleUrlSearch($message)
    {
        $message = explode(' ', trim($message));
        $url = $message[2];
        $scan_id = $this->service->urlSearch($url);
        $results = $this->service->urlResult($url, array_get($scan_id, 'scan_id'));

        # Sanity check for the returning response from the API.
        if (is_bool($results) || count(array_get($results, 'positives')) === 0) {
            $reply [] = sprintf('*No results found for:* %s.' . PHP_EOL .
                ' A scan is most likely being run right now, please try with the same URL in a short while', $url);

            $message = implode(PHP_EOL, $reply);
            return $message;
        }
        
        # Retrieves the results of malicious domains from the specified URL
        else {
            $positives = array_get($results, 'positives');
            $total = array_get($results, 'total');

            $reply = [
                sprintf('*Waiting for results*'),
                sprintf('*Scan Finished*: %s thinks that this site might be malicious, out of %s vendors', $positives, $total)
            ];

            $message = implode(PHP_EOL, $reply);
            return $message;
        }
    }
}
