<?php

namespace Pillus\Secbot\Modules\Wpscan;

use Pillus\Secbot\Modules\Wpscan\Wpscan;

/**
* This module intergrates with the API of WPSCAN DB, to look for vulnerabilities related
* to wordpress themes, versions and plugins.
**/

class Plugin
{

    /**
     * @var Botman
     */
    protected $discord;

    /**
     * @var Wpscan
     */
    protected $service;

    public function __construct()
    {
        $this->service = new Wpscan;
    }

    public function setup($discord)
    {
        $this->discord = $discord;

        return $this;
    }

    /**
    * Initiates all available commands used by the Shodan module.
    **/

    public function init()
    {
        $command = $this->discord->registerCommand('wpscan', function () {
        });
        
        $command->registerSubCommand('version', function ($message, $params) {
            $reply = self::handleVersionSearch($message->content);
            return $reply;
        });
        
        $command->registerSubCommand('plugin', function ($message, $params) {
            $reply = self::handlePluginSearch($message->content);
            return $reply;
        });
    }

    /**
    * Lists vulnerabilities related to specific wordpress versions.
    **/

    public function handleVersionSearch($message)
    {
        $message = explode(' ', trim($message));
        $version = $message[2];
        $results = $this->service->versionSearch(str_replace('.', '', $version));
        
        # Sanity check for the returning response from the API.
        if (is_bool($results) || count(array_get($results, $version)) === 0) {
            $reply[] = sprintf('*No results found for version:* %s', $version);
            $message = implode(PHP_EOL, $reply);
            return $message;
        }

        # Returns all vulnerabilities related to a specific wordpress version
        else {
            $reply = [
                sprintf('Your Results for Wordpress version: %s', $version),
            ];

            $version = array_get($results, $version, []);

            $vulns = array_get($version, 'vulnerabilities');
            $reply[] = sprintf('Vulnerabilities found: %s', count($vulns)) . PHP_EOL;

            foreach ($vulns as $vuln) {
                $title = array_get($vuln, 'title');
                $date = date('d/m/Y', strtotime(array_get($vuln, 'published_date')));
                $urls = array_get($vuln, 'references.url');
                $fixed = array_get($vuln, 'fixed_in');

                $reply[] = sprintf('*Title*: ', $title);
                $reply[] = sprintf('*Published at*: %s', $date);

                foreach ($urls as $url) {
                    $reply[] = sprintf('*Url*: %s', $url);
                }

                $reply[] = sprintf('*Fixed In*: %s', $fixed) . PHP_EOL;
            }
            $message = implode(PHP_EOL, array_slice($reply, 0, 30));
            return $message;
        }
    }

    /**
    * Lists vulnerabilities related to specific wordpress plugin.
    **/

    public function handlePluginSearch($message)
    {
        $message = explode(' ', trim($message));
        $plugin = $message[2];
        $results = $this->service->pluginSearch($plugin);

        # Sanity check for the returning response from the API.
        if (is_bool($results) || count(array_get($results, $plugin)) === 0) {
            $reply[] = sprintf('*No Vulnerabilities found for this plugin*');

            $message = implode(PHP_EOL, $reply);
            return $message;
        }

        # Returns all vulnerabilities related to a specific wordpress plugin
        else {
            $vulns = array_get($results, $plugin . '.vulnerabilities');

            $reply = [
            sprintf('*Your Results for Wordpress Plugin:* %s', $plugin),
            sprintf('*Vulnerabilities found: %s*', count($vulns)) . PHP_EOL
            ];

            foreach ($vulns as $vuln) {
                $title = array_get($vuln, 'title');
                $date = date('d/m/Y', strtotime(array_get($vuln, 'published_date')));
                $urls = array_get($vuln, 'references.url', []);
                $fixed = array_get($vuln, 'fixed_in');

                $reply[] = sprintf('*Title*: %s', $title);
                $reply[] = sprintf('*Published at*: %s', $date);

                foreach ($urls as $url) {
                    $reply[] = sprintf('*Url*: %s', $url);
                }

                if (count($fixed) === 0) {
                    $fixed = 'Unknown';
                }

                $reply[] = sprintf('*Fixed In*: %s', $fixed) . PHP_EOL;
            }

            $message = implode(PHP_EOL, array_slice($reply, 0, 30));
            return $message;
        }
    }
}
